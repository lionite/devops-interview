#### Prueba 1 - Diagrama de Red

<p align="center"><img src="esquema_craftech.png" /></p>


Se plantea una arquitectura donde se usa un Elastic Load Balancing de Aplicación, que va a redireccionar las peticiones de los usuarios hacia la aplicación django. La aplicación cuenta con backend y frontend distribuido en dos containers. Se propone que los containers se despleguen usando Fargate. Con Fargate se puede configurar facilmente el escalamiento de las aplicaciones, y se evita los problemas de configuración de un servidor. Para la base de datos relacional se usa PostgreSQL y Elasticache como base de datos no relacional.

#### Prueba 2 - Despliegue de una aplicación Django y React.js

## Build

Para buildear la aplicación hay que buildear dos imágenes:

**Backend:**

```sh
  cd backend
  docker build -t registry.gitlab.com/lionite/devops-interview/backend:develop .
```

**Frontend:**

```sh
  cd frontend
  docker build -t registry.gitlab.com/lionite/devops-interview/frontend:develop .
```

## Deploy

Para deployar localmente usando kubernetes:

1. Instalar minikube: https://minikube.sigs.k8s.io/docs/start/
2. Levantar el cluster de minikube con:
```sh
  minikube start
```
3. Crear los secretos que se van a usar:
```sh
  cd k8s
  kubectl apply -f 02-secrets.yaml
```
4.
Una vez arriba se puede ingresar a la aplicación:

```sh
  http://localhost:8000
```

## CI/CD

Se usó gitlab CI para realizar el pipeline de build/deploy de la aplicación
